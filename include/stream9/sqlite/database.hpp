#ifndef STREAM9_SQLITE_DATABASE_HPP
#define STREAM9_SQLITE_DATABASE_HPP

#include "error.hpp"
#include "namespace.hpp"
#include "statement.hpp"
#include "transaction.hpp"

#include <filesystem>
#include <string_view>
#include <utility>

#include <stream9/cstring_ptr.hpp>

struct sqlite3;

namespace stream9::sqlite {

class database
{
public:
    using handle_t = ::sqlite3*;

public:
    // essential
    database(cstring_ptr path);

    database(database const&) = delete;
    database& operator=(database const&) = delete;

    database(database&&) noexcept;
    database& operator=(database&&) noexcept;

    ~database() noexcept;

    // accessor
    handle_t handle() const noexcept { return m_handle; }

    // query
    int changes() const noexcept; // number of affected rows in recent query
    int64_t last_insert_rowid() const noexcept; // return 0 if no successful INSERT has happened
    int last_error_code() const noexcept;
    bool is_readonly() const noexcept;

    // command
    template<typename Str, typename ...Args>
    statement exec(Str &&sql, Args&&... args);

    statement prepare(std::string_view sql);

    transaction begin_transaction();

    void busy_timeout(int ms);

private:
    void close() noexcept;

private:
    handle_t m_handle; // null after moved, otherwise non-null
};

template<typename Str, typename ...Args>
statement database::
exec(Str &&sql, Args&&... args)
{
    try {
        auto stmnt = prepare(std::forward<Str>(sql));
        stmnt.exec(std::forward<Args>(args)...);

        return stmnt;
    }
    catch (...) {
        rethrow_error();
    }
}

inline statement database::
prepare(std::string_view sql)
{
    try {
        return { *this, sql };
    }
    catch (...) {
        rethrow_error();
    }
}

inline transaction database::
begin_transaction()
{
    try {
        return *this;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::sqlite

#endif //STREAM9_SQLITE_DATABASE_HPP
