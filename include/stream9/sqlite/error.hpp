#ifndef STREAM9_SQLITE_ERROR_HPP
#define STREAM9_SQLITE_ERROR_HPP

#include <system_error>
#include <source_location>

#include <stream9/errors.hpp>

#include <stream9/json.hpp>

namespace stream9::sqlite {

class database;

class error : public stream9::error
{
public:
    error(database&,
          std::source_location = std::source_location::current());

    error(database&,
          json::object&& context,
          std::source_location = std::source_location::current());
};

std::error_category& error_category() noexcept;

} // namespace stream9::sqlite

#endif // STREAM9_SQLITE_ERROR_HPP
