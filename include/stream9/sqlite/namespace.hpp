#ifndef STREAM9_SQLITE_NAMESPACE_HPP
#define STREAM9_SQLITE_NAMESPACE_HPP

namespace stream9::strings {}
namespace stream9::json {}

namespace stream9::sqlite {

namespace str { using namespace stream9::strings; }
namespace json { using namespace stream9::json; }

} // namespace stream9::sqlite

#endif // STREAM9_SQLITE_NAMESPACE_HPP
