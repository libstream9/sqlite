#ifndef STREAM9_SQLITE_ROW_HPP
#define STREAM9_SQLITE_ROW_HPP

#include <string_view>
#include <span>

#include <stream9/safe_integer.hpp>

namespace stream9::sqlite {

class statement;

class row
{
public:
    using column_index_t = stream9::safe_integer<int, 0>;

public:
    row(statement const&) noexcept;

    int                   get_int(column_index_t) const noexcept;
    int64_t               get_int64(column_index_t) const noexcept;
    double                get_double(column_index_t) const noexcept;
    std::string_view      get_text(column_index_t) const noexcept;
    std::span<char const> get_blob(column_index_t) const noexcept;

private:
    statement const* m_stmt; // non-null
};

} // namespace stream9::sqlite

#endif // STREAM9_SQLITE_ROW_HPP
