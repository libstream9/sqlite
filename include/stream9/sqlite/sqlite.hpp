#ifndef STREAM9_SQLITE_SQLITE_HPP
#define STREAM9_SQLITE_SQLITE_HPP

#include <sqlite3.h>

namespace stream9::sqlite {

/*
 * 0: single-thread mode
 * 1: serialized mode
 * 2: multi-thread mode
 */
int threadsafe() noexcept;

} // namespace stream9::sqlite

#endif // STREAM9_SQLITE_SQLITE_HPP
