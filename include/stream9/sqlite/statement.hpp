#ifndef STREAM9_SQLITE_STATEMENT_HPP
#define STREAM9_SQLITE_STATEMENT_HPP

#include "error.hpp"
#include "row.hpp"
#include "statement_iterator.hpp"

#include <cassert>
#include <concepts>
#include <span>
#include <string_view>
#include <utility>

#include <stream9/safe_integer.hpp>

struct sqlite3_stmt;

namespace stream9::sqlite {

class blob_view;
class database;

class statement
{
public:
    using value_type = row;
    using handle_t = ::sqlite3_stmt*;
    using const_iterator = statement_iterator;
    using param_index_t = stream9::safe_integer<int, 1>;

public:
    // essential
    statement(sqlite::database&, std::string_view sql);
    ~statement() noexcept;

    statement(statement const&) = delete;
    statement& operator=(statement const&) = delete;

    statement(statement&&) noexcept;
    statement& operator=(statement&&) noexcept;

    // accessor
    sqlite::database& database() noexcept;
    handle_t handle() const noexcept;

    // query
    bool done() const noexcept;
    row current_row() const noexcept;

    // iterator
    const_iterator begin() noexcept;
    const_iterator end() noexcept;

    // command
    template<typename ...Args>
    void exec(Args&&... args);

    void exec();

    void step();

    template<typename... Args>
    void bind(Args&&... args);

    void bind_to(param_index_t index, int);
    void bind_to(param_index_t index, int64_t);
    void bind_to(param_index_t index, double);
    void bind_to(param_index_t index, std::string_view);
    void bind_to(param_index_t index, std::nullptr_t);

    template<typename T>
        requires std::convertible_to<T, std::span<char const>>
              && (!std::convertible_to<T, std::string_view>)
    void bind_to(param_index_t index, T&& v);

    void reset();
    void clear_bindings();

    // operator
    void operator()();

    template<typename... Args>
    void operator()(Args&&... args);

private:
    template<typename T, typename... Args>
    void bind_va(param_index_t const index, T&& first, Args&&... rest);

    void bind_va(param_index_t) {}

    void bind_blob_to(param_index_t index, std::span<char const>);

    void finalize() noexcept;

private:
    friend class statement_iterator;

    sqlite::database* m_db; // non-null
    handle_t m_handle; // nullptr after moved, otherwise non-null
    bool m_done = false;
};

} // namespace stream9::sqlite

#include "statement.ipp"

#endif // STREAM9_SQLITE_STATEMENT_HPP
