#ifndef STREAM9_SQLITE_STATEMENT_IPP
#define STREAM9_SQLITE_STATEMENT_IPP

namespace stream9::sqlite {

inline sqlite::database& statement::
database() noexcept
{
    return *m_db;
}

inline statement::handle_t statement::
handle() const noexcept
{
    return m_handle;
}

inline bool statement::
done() const noexcept
{
    return m_done;
}

inline row statement::
current_row() const noexcept
{
    return *this;
}

inline statement::const_iterator statement::
begin() noexcept
{
    return { *this };
}

inline statement::const_iterator statement::
end() noexcept
{
    return {};
}

template<typename ...Args>
void statement::
exec(Args&&... args)
{
    try {
        reset();
        bind(std::forward<Args>(args)...);
        exec();
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename... Args>
void statement::
bind(Args&&... args)
{
    try {
        bind_va(1, args...);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T>
    requires std::convertible_to<T, std::span<char const>>
          && (!std::convertible_to<T, std::string_view>)
void statement::
bind_to(param_index_t index, T&& v)
{
    try {
        bind_blob_to(index, std::forward<T>(v));
    }
    catch (...) {
        rethrow_error();
    }
}

inline void statement::
operator()()
{
    try {
        exec();
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename... Args>
void statement::
operator()(Args&&... args)
{
    try {
        exec(std::forward<Args>(args)...);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename... Args>
void statement::
bind_va(param_index_t const index, T&& first, Args&&... rest)
{
    bind_to(index, std::forward<T>(first));
    bind_va(index+1, std::forward<Args>(rest)...);
}

} // namespace stream9::sqlite

#endif // STREAM9_SQLITE_STATEMENT_IPP
