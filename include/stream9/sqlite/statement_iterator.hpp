#ifndef STREAM9_SQLITE_STATEMENT_ITERATOR_HPP
#define STREAM9_SQLITE_STATEMENT_ITERATOR_HPP

#include "row.hpp"

#include <iterator>

#include <stream9/iterators.hpp>

namespace stream9::sqlite {

class statement;

class statement_iterator
    : public stream9::iterators::iterator_facade<statement_iterator,
                                      std::input_iterator_tag,
                                      row >
{
public:
    statement_iterator(sqlite::statement&) noexcept;
    statement_iterator() noexcept {}

    // accessor
    sqlite::statement& statement() noexcept { return *m_stmt; }

private:
    friend class stream9::iterators::iterator_core_access;

    // required from stream9::iterators::iterator_facade
    row dereference() const noexcept;
    void increment();
    bool equal(statement_iterator const& other) const noexcept;

private:
    sqlite::statement* m_stmt = nullptr;
};

} // namespace stream9::sqlite

#endif // STREAM9_SQLITE_STATEMENT_ITERATOR_HPP
