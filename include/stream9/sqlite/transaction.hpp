#ifndef STREAM9_SQLITE_TRANSACTION_HPP
#define STREAM9_SQLITE_TRANSACTION_HPP

namespace stream9::sqlite {

class database;

class transaction
{
public:
    transaction(database&);

    transaction(transaction const&) = delete;
    transaction& operator=(transaction const&) = delete;

    transaction(transaction&&) noexcept;
    transaction& operator=(transaction&&) noexcept;

    ~transaction() noexcept;

    void commit();
    void rollback();

    void swap(transaction&) noexcept;

private:
    database* m_db; // non-null
    bool m_open = false;
};

} // namespace stream9::sqlite

#endif // STREAM9_SQLITE_TRANSACTION_HPP
