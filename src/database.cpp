#include <stream9/sqlite/database.hpp>

#include <stream9/sqlite/error.hpp>

#include <cassert>

#include <sqlite3.h>

#include <stream9/log.hpp>

namespace stream9::sqlite {

database::
database(str::cstring_ptr const path)
{
    ::sqlite3 *handle = nullptr;
    auto const rc = ::sqlite3_open(path, &handle);
    if (rc != SQLITE_OK) {
        throw error { *this, {
            { "path", path },
        } };
    }

    m_handle = handle;
    assert(m_handle);
}

database::
~database() noexcept
{
    close();
}

database::
database(database&& other) noexcept
{
    assert(other.m_handle);

    m_handle = other.m_handle;
    other.m_handle = nullptr;

    assert(m_handle);
}

database& database::
operator=(database&& rhs) noexcept
{
    assert(rhs.m_handle);

    close();
    m_handle = rhs.m_handle;
    rhs.m_handle = nullptr;

    assert(m_handle);

    return *this;
}

int database::
changes() const noexcept
{
    return ::sqlite3_changes(m_handle);
}

int64_t database::
last_insert_rowid() const noexcept
{
    return ::sqlite3_last_insert_rowid(m_handle);
}

int database::
last_error_code() const noexcept
{
    return ::sqlite3_extended_errcode(m_handle);
}

bool database::
is_readonly() const noexcept
{
    return ::sqlite3_db_readonly(m_handle, "main");
}

void database::
busy_timeout(int const ms)
{
    auto const rc = ::sqlite3_busy_timeout(m_handle, ms);
    if (rc != SQLITE_OK) {
        throw error { *this, {
            { "ms", ms },
        } };
    }
}

void database::
close() noexcept
{
    if (!m_handle) return;

    auto const rc = ::sqlite3_close_v2(m_handle);
    if (rc != SQLITE_OK) {
        error const e { *this };
        print_error(log::warn(), e);
    }

    m_handle = nullptr;
}

// database

} // namespace stream9::sqlite
