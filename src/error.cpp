#include <stream9/sqlite/error.hpp>

#include <stream9/sqlite/database.hpp>

#include <sqlite3.h>

#include <stream9/strings/stream.hpp> // operator<<

namespace stream9::sqlite {

static std::string
error_code_to_symbol(int const e)
{
    using str::operator<<;

    std::string sym;

#define STR(X) #X
#define ENTRY(X) case SQLITE_##X: sym = STR(SQLITE_##X); break
    switch (e) {
        ENTRY(OK);
        ENTRY(ERROR);
        ENTRY(INTERNAL);
        ENTRY(PERM);
        ENTRY(ABORT);
        ENTRY(BUSY);
        ENTRY(LOCKED);
        ENTRY(NOMEM);
        ENTRY(READONLY);
        ENTRY(INTERRUPT);
        ENTRY(IOERR);
        ENTRY(CORRUPT);
        ENTRY(NOTFOUND);
        ENTRY(FULL);
        ENTRY(CANTOPEN);
        ENTRY(PROTOCOL);
        ENTRY(EMPTY);
        ENTRY(SCHEMA);
        ENTRY(TOOBIG);
        ENTRY(CONSTRAINT);
        ENTRY(MISMATCH);
        ENTRY(MISUSE);
        ENTRY(NOLFS);
        ENTRY(AUTH);
        ENTRY(FORMAT);
        ENTRY(RANGE);
        ENTRY(NOTADB);
        ENTRY(NOTICE);
        ENTRY(WARNING);
        ENTRY(ROW);
        ENTRY(DONE);
        ENTRY(ERROR_MISSING_COLLSEQ);
        ENTRY(ERROR_RETRY);
        ENTRY(ERROR_SNAPSHOT);
        ENTRY(IOERR_READ);
        ENTRY(IOERR_SHORT_READ);
        ENTRY(IOERR_WRITE);
        ENTRY(IOERR_FSYNC);
        ENTRY(IOERR_DIR_FSYNC);
        ENTRY(IOERR_TRUNCATE);
        ENTRY(IOERR_FSTAT);
        ENTRY(IOERR_UNLOCK);
        ENTRY(IOERR_RDLOCK);
        ENTRY(IOERR_DELETE);
        ENTRY(IOERR_BLOCKED);
        ENTRY(IOERR_NOMEM);
        ENTRY(IOERR_ACCESS);
        ENTRY(IOERR_CHECKRESERVEDLOCK);
        ENTRY(IOERR_LOCK);
        ENTRY(IOERR_CLOSE);
        ENTRY(IOERR_DIR_CLOSE);
        ENTRY(IOERR_SHMOPEN);
        ENTRY(IOERR_SHMSIZE);
        ENTRY(IOERR_SHMLOCK);
        ENTRY(IOERR_SHMMAP);
        ENTRY(IOERR_SEEK);
        ENTRY(IOERR_DELETE_NOENT);
        ENTRY(IOERR_MMAP);
        ENTRY(IOERR_GETTEMPPATH);
        ENTRY(IOERR_CONVPATH);
        ENTRY(IOERR_VNODE);
        ENTRY(IOERR_AUTH);
        ENTRY(IOERR_BEGIN_ATOMIC);
        ENTRY(IOERR_COMMIT_ATOMIC);
        ENTRY(IOERR_ROLLBACK_ATOMIC);
        ENTRY(IOERR_DATA);
        ENTRY(IOERR_CORRUPTFS);
        ENTRY(LOCKED_SHAREDCACHE);
        ENTRY(LOCKED_VTAB);
        ENTRY(BUSY_RECOVERY);
        ENTRY(BUSY_SNAPSHOT);
        ENTRY(BUSY_TIMEOUT);
        ENTRY(CANTOPEN_NOTEMPDIR);
        ENTRY(CANTOPEN_ISDIR);
        ENTRY(CANTOPEN_FULLPATH);
        ENTRY(CANTOPEN_CONVPATH);
        ENTRY(CANTOPEN_DIRTYWAL);
        ENTRY(CANTOPEN_SYMLINK);
        ENTRY(CORRUPT_VTAB);
        ENTRY(CORRUPT_SEQUENCE);
        ENTRY(CORRUPT_INDEX);
        ENTRY(READONLY_RECOVERY);
        ENTRY(READONLY_CANTLOCK);
        ENTRY(READONLY_ROLLBACK);
        ENTRY(READONLY_DBMOVED);
        ENTRY(READONLY_CANTINIT);
        ENTRY(READONLY_DIRECTORY);
        ENTRY(ABORT_ROLLBACK);
        ENTRY(CONSTRAINT_CHECK);
        ENTRY(CONSTRAINT_COMMITHOOK);
        ENTRY(CONSTRAINT_FOREIGNKEY);
        ENTRY(CONSTRAINT_FUNCTION);
        ENTRY(CONSTRAINT_NOTNULL);
        ENTRY(CONSTRAINT_PRIMARYKEY);
        ENTRY(CONSTRAINT_TRIGGER);
        ENTRY(CONSTRAINT_UNIQUE);
        ENTRY(CONSTRAINT_VTAB);
        ENTRY(CONSTRAINT_ROWID);
        ENTRY(CONSTRAINT_PINNED);
        ENTRY(CONSTRAINT_DATATYPE);
        ENTRY(NOTICE_RECOVER_WAL);
        ENTRY(NOTICE_RECOVER_ROLLBACK);
        ENTRY(WARNING_AUTOINDEX);
        ENTRY(AUTH_USER);
        ENTRY(OK_LOAD_PERMANENTLY);
        ENTRY(OK_SYMLINK);
        default:
            sym << "unknwon (" << e << ")";
            break;
    }
#undef ENTRY
#undef STR

    return sym;
}

static json::object&&
make_context(database const& db, json::object&& cxt)
{
    cxt["error_string"]
        = ::sqlite3_errstr(::sqlite3_extended_errcode(db.handle()));
    cxt["message"] = ::sqlite3_errmsg(db.handle());

    auto const off = ::sqlite3_error_offset(db.handle());
    if (off != -1) {
        cxt["offset"] = off;
    }

    return std::move(cxt);
}

static json::object
make_context(database const& db)
{
    json::object cxt;

    make_context(db, std::move(cxt));

    return cxt;
}

static std::error_code
make_error_code(database const& db)
{
    return {
        ::sqlite3_extended_errcode(db.handle()),
        error_category()
    };
}

/*
 * error
 */
error::
error(database &db, std::source_location loc)
    : stream9::error { make_error_code(db), make_context(db), std::move(loc) }
{}

error::
error(database& db,
      json::object&& context,
      std::source_location loc)
    : stream9::error { make_error_code(db),
                       make_context(db, std::move(context)),
                       std::move(loc) }
{}

/*
 * error_category
 */
std::error_category&
error_category() noexcept
{
    struct sqlite_error_category : std::error_category {
        const char* name() const noexcept override
        {
            return "SQLite";
        }

        std::string message(int const result_code) const
        {
            return error_code_to_symbol(result_code);
        }
    };

    static sqlite_error_category instance;

    return instance;
}

} // namespace stream9::sqlite
