#include <stream9/sqlite/row.hpp>

#include <stream9/sqlite/statement.hpp>

#include <string_view>

#include <sqlite3.h>

namespace stream9::sqlite {

row::
row(statement const& stmt) noexcept
    : m_stmt { &stmt }
{}

int row::
get_int(column_index_t const column) const noexcept
{
    return ::sqlite3_column_int(m_stmt->handle(), column);
}

int64_t row::
get_int64(column_index_t const column) const noexcept
{
    return ::sqlite3_column_int64(m_stmt->handle(), column);
}

double row::
get_double(column_index_t const column) const noexcept
{
    return ::sqlite3_column_double(m_stmt->handle(), column);
}

std::string_view row::
get_text(column_index_t const column) const noexcept
{
    return {
        reinterpret_cast<char const*>(
            ::sqlite3_column_text(m_stmt->handle(), column)),
        static_cast<size_t>(
            ::sqlite3_column_bytes(m_stmt->handle(), column))
    };
}

std::span<char const> row::
get_blob(column_index_t const column) const noexcept
{
    return {
        reinterpret_cast<char const*>(
            ::sqlite3_column_blob(m_stmt->handle(), column)),
        static_cast<size_t>(
            ::sqlite3_column_bytes(m_stmt->handle(), column))
    };
}

} // namespace stream9::sqlite
