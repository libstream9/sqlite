#include <stream9/sqlite/sqlite.hpp>

namespace stream9::sqlite {

int
threadsafe() noexcept
{
    return ::sqlite3_threadsafe();
}

} // namespace stream9::sqlite
