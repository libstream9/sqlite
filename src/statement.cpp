#include <stream9/sqlite/statement.hpp>

#include <stream9/sqlite/database.hpp>
#include <stream9/sqlite/error.hpp>

#include <climits>

#include <sqlite3.h>

#include <stream9/log.hpp>

namespace stream9::sqlite {

statement::
statement(sqlite::database &db, std::string_view const sql)
    : m_db { &db }
    , m_handle { nullptr }
{
    assert(sql.size() <= INT_MAX);

    char const* tail = nullptr;
    auto const rc = ::sqlite3_prepare_v2(
        db.handle(),
        sql.data(),
        static_cast<int>(sql.size()),
        &m_handle,
        &tail
    );
    if (rc != SQLITE_OK) {
        throw error { db, {
            { "sql", sql },
        } };
    }

    assert(m_db);
    assert(m_handle);
}

statement::
~statement() noexcept
{
    finalize();
}

statement::
statement(statement&& other) noexcept
    : m_db { other.m_db }
    , m_done { other.m_done }
{
    assert(other.m_handle);

    m_handle = other.m_handle;
    other.m_handle = nullptr;

    assert(m_db);
    assert(m_handle);
}

statement& statement::
operator=(statement&& rhs) noexcept
{
    m_db = rhs.m_db;
    m_done = rhs.m_done;

    assert(rhs.m_handle);

    finalize();
    m_handle = rhs.m_handle;
    rhs.m_handle = nullptr;

    assert(m_db);
    assert(m_handle);

    return *this;
}

void statement::
exec()
{
    try {
        assert(m_handle);

        step();
    }
    catch (...) {
        rethrow_error();
    }
}

void statement::
step()
{
    assert(m_handle);

    auto const rc = ::sqlite3_step(m_handle);
    if (rc == SQLITE_DONE) {
        m_done = true;
    }
    else if (rc == SQLITE_ROW) {
        m_done = false;
    }
    else {
        throw error(*m_db);
    }
}

void statement::
bind_to(param_index_t const index, int const value)
{
    assert(m_handle);

    auto const rc = ::sqlite3_bind_int(m_handle, index, value);
    if (rc != SQLITE_OK) {
        throw error(*m_db, {
            { "index", index },
            { "value", value },
        });
    }
}

void statement::
bind_to(param_index_t const index, int64_t const value)
{
    assert(m_handle);

    auto const rc = ::sqlite3_bind_int64(m_handle, index, value);
    if (rc != SQLITE_OK) {
        throw error(*m_db, {
            { "index", index },
            { "value", value },
        });
    }
}

void statement::
bind_to(param_index_t const index, double const value)
{
    assert(m_handle);

    auto const rc = ::sqlite3_bind_double(m_handle, index, value);
    if (rc != SQLITE_OK) {
        throw error(*m_db, {
            { "index", index },
            { "value", value },
        });
    }
}

void statement::
bind_to(param_index_t const index, std::string_view const str)
{
    assert(m_handle);
    assert(str.size() <= INT_MAX);

    auto const rc = ::sqlite3_bind_text(
        m_handle,
        index,
        str.data(), static_cast<int>(str.size()),
        SQLITE_STATIC
    );
    if (rc != SQLITE_OK) {
        throw error(*m_db, {
            { "index", index },
            { "str", str },
        });
    }
}

void statement::
bind_to(param_index_t const index, std::nullptr_t)
{
    assert(m_handle);

    auto const rc = ::sqlite3_bind_null(m_handle, index);
    if (rc != SQLITE_OK) {
        throw error(*m_db, {
            { "index", index },
        });
    }
}

void statement::
reset()
{
    assert(m_handle);

    auto const rc = ::sqlite3_reset(m_handle);
    if (rc != SQLITE_OK) {
        throw error(*m_db);
    }
}

void statement::
clear_bindings()
{
    assert(m_handle);

    auto const rc = ::sqlite3_clear_bindings(m_handle);
    if (rc != SQLITE_OK) {
        throw error(*m_db);
    }
}

void statement::
bind_blob_to(param_index_t const index, std::span<char const> const blob)
{
    assert(m_handle);

    auto const rc = ::sqlite3_bind_blob64(
            m_handle, index, blob.data(), blob.size(), SQLITE_STATIC);
    if (rc != SQLITE_OK) {
        throw error(*m_db, {
            { "index", index },
        });
    }
}

void statement::
finalize() noexcept
{
    if (!m_handle) return;

    auto const rc = ::sqlite3_finalize(m_handle);
    if (rc != SQLITE_OK) {
        error const e { *m_db };
        print_error(log::warn(), e);
    }

    m_handle = nullptr;
}

} // namespace stream9::sqlite
