#include <stream9/sqlite/statement_iterator.hpp>

#include <stream9/sqlite/error.hpp>
#include <stream9/sqlite/row.hpp>
#include <stream9/sqlite/statement.hpp>

#include <cassert>

#include <sqlite3.h>

namespace stream9::sqlite {

statement_iterator::
statement_iterator(sqlite::statement& stmnt) noexcept
    : m_stmt { &stmnt }
{}

row statement_iterator::
dereference() const noexcept
{
    assert(m_stmt);

    return { *m_stmt };
}

void statement_iterator::
increment()
{
    try {
        m_stmt->step();
    }
    catch (...) {
        rethrow_error();
    }
}

bool statement_iterator::
equal(statement_iterator const& other) const noexcept
{
    // comparing iterators that come from different sequence is undefined
    // ISO C++ standard 24.2.5.2

    if (m_stmt && !other.m_stmt) {
        return m_stmt->done();
    }
    else if (!m_stmt && other.m_stmt) {
        return other.m_stmt->done();
    }
    else if (m_stmt && other.m_stmt) {
        return m_stmt->done() == other.m_stmt->done();
    }
    else { // !m_stmt && !other.m_stmt
        return true;
    }
}

} // namespace stream9::sqlite
