#include <stream9/sqlite/transaction.hpp>

#include <stream9/sqlite/database.hpp>
#include <stream9/sqlite/error.hpp>

#include <utility>

#include <stream9/log.hpp>

namespace stream9::sqlite {

transaction::
transaction(database& db)
    : m_db { &db }
{
    try {
        m_db->exec("BEGIN TRANSACTION");
        m_open = true;
    }
    catch (...) {
        rethrow_error();
    }
}

transaction::
transaction(transaction&& other) noexcept
    : m_db { other.m_db }
    , m_open { other.m_open }
{
    other.m_db = nullptr;
    other.m_open = false;
}

transaction& transaction::
operator=(transaction&& rhs) noexcept
{
    transaction tmp { std::move(rhs) };
    swap(tmp);

    return *this;
}

void transaction::
swap(transaction& other) noexcept
{
    using std::swap;
    swap(m_db, other.m_db);
    swap(m_open, other.m_open);
}

transaction::
~transaction() noexcept
{
    try {
        rollback();
    }
    catch (...) {
        print_error(log::warn());
    }
}

void transaction::
commit()
{
    try {
        if (m_open) {
            m_db->exec("COMMIT");
            m_open = false;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void transaction::
rollback()
{
    try {
        if (m_open) {
            m_db->exec("ROLLBACK");
            m_open = false;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::sqlite
